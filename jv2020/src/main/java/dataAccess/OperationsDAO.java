package dataAccess;

import java.util.List;

import models.Identifiable;

public interface OperationsDAO {

	public Identifiable find(String id);
	public List<Identifiable> findAll();
	public void create(Object obj) throws DataAccessException;
	public Identifiable delete(String id) throws DataAccessException;
	public Identifiable delete(Object obj) throws DataAccessException;
	public Identifiable update(Object obj) throws DataAccessException;
	public String toStringDatos();
	public String toStringIds();
	public void deleteAll();
	
}
